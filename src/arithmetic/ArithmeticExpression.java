package arithmetic;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class ArithmeticExpression {
  private ArrayList<String> operators;
  private ArrayList<Double> operands;

  public ArrayList<String> getOperators() {
    return operators;
  }

  public ArrayList<Double> getOperands() {
    return operands;
  }

  public ArithmeticExpression() {
    this.operators = new ArrayList<String>();
    this.operands = new ArrayList<Double>();
  }

  public void clearExpression() {
    this.operators = new ArrayList<String>();
    this.operands = new ArrayList<Double>();
  }

  public String getExpressionString(String filename) throws FileNotFoundException {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    InputStream stream = classLoader.getResourceAsStream(filename);

    Scanner scanner = new Scanner(stream);
    String result = scanner.nextLine();
    scanner.close();
    return result;
  }

  public void parse(String filename) throws FileNotFoundException, IllegalArgumentException {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    InputStream stream = classLoader.getResourceAsStream(filename);

    Scanner scanner = new Scanner(stream);
    String symbol;
    boolean moved = false;
    int position = 0;

    while (scanner.hasNext()) {
      if (scanner.hasNextInt()) {
        if (moved) {
          operands.add(position + 1, (double) scanner.nextInt());
        } else
          operands.add((double) scanner.nextInt());
      } else if (scanner.hasNextDouble()) {
        if (moved) {
          operands.add(position + 1, scanner.nextDouble());
        } else
          operands.add(scanner.nextDouble());

      } else {
        symbol = scanner.next();

        switch (symbol) {
          case "*":
          case "/": {
            if (operators.size() > 0) {
              position = operators.size() - 1;
              while (!(operators.get(position).equals("*") || operators.get(position).equals("/"))
                  && position > 0) {
                position--;
              }

              if (position != operators.size() - 1
                  || (operators.get(position).equals("+") || operators.get(position).equals("-"))) {
                operators.add(position, symbol);
                Double operandTemp = operands.get(operands.size() - 1);
                operands.add(position, operandTemp);
                operands.remove(operands.size() - 1);
                moved = true;
              } else {
                operators.add(symbol);
                moved = false;
              }
            } else {
              operators.add(symbol);
              moved = false;
            }
          }
            break;
          case "+":
          case "-":
            operators.add(symbol);
            moved = false;
            break;
          default:
            throw new IllegalArgumentException("was found illegal symbol");
        }
      }
    }

    scanner.close();
  }


  public double calculate() {
    if (operands.size() - operators.size() != 1) {
      if (operands.size() == 0)
        throw new IllegalArgumentException("empty string");
      if (operands.size() > operators.size())
        throw new IllegalArgumentException("excess operands");
      if (operands.size() <= operators.size())
        throw new IllegalArgumentException("excess operators");
    }
    
    double result = operands.get(0);
    for (int i = 1; i < operands.size(); i++) {
      switch (operators.get(i - 1)) {
        case "*":
          result = result * operands.get(i);
          break;
        case "+":
          result = result + operands.get(i);
          break;
        case "/":
          if (operands.get(i) == 0)
            throw new IllegalArgumentException("divide by zero");
          result = result / operands.get(i);
          break;
        case "-":
          result = result - operands.get(i);
          break;
      }
    }
    return result;
  }
}
