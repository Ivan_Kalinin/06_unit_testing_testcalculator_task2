package program;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import arithmetic.ArithmeticExpression;

public class Program {
  private static String filename = "resources/expression.txt";
  public static double result;

  public static void main(String[] args) {
    ArithmeticExpression expression = new ArithmeticExpression();
    
    try {
      expression.parse(filename);
      result = expression.calculate();

      StringBuilder outString = new StringBuilder();
      outString.append(expression.getExpressionString(filename)).append(" = ")
          .append(String.format("%.3f", result));
      System.out.println(outString);
    } catch (FileNotFoundException | IllegalArgumentException e) {
      e.printStackTrace();
    }

    // printing of expression containers
    Program.<Double>printArray(expression.getOperands());
    Program.<String>printArray(expression.getOperators());
  }

  private static <U> void printArray(ArrayList<U> objs) {
    StringBuilder string = new StringBuilder();
    for (U ob : objs) {
      string.append(ob.toString()).append(" ");
    }

    System.out.println(string);
  }


}
