package positive_tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;

import java.io.FileNotFoundException;

import org.junit.BeforeClass;
import org.junit.Test;

import arithmetic.ArithmeticExpression;

public class TestCalculatorOperations {
  private static double epsilon;
  private static ArithmeticExpression expression;

  @BeforeClass
  public static void ini() {
    epsilon = 1e-10;
    expression = new ArithmeticExpression();
  }

  @Test
  public void testAddition() throws FileNotFoundException, IllegalArgumentException {
    expression.parse("resources/addition.txt");
    assertThat(expression.calculate(), closeTo(1 + 3.4D, epsilon));
    expression.clearExpression();

    expression.parse("resources/addition2.txt");
    assertThat(expression.calculate(), closeTo(33 + 2 + 34.4D, epsilon));
    expression.clearExpression();
  }

  @Test
  public void testSubstraction() throws FileNotFoundException, IllegalArgumentException {
    expression.parse("resources/substraction.txt");
    assertThat(expression.calculate(), closeTo(54 - 28, epsilon));
    expression.clearExpression();

    expression.parse("resources/substraction2.txt");
    assertThat(expression.calculate(), closeTo(54 - 28 - 5, epsilon));
    expression.clearExpression();
  }

  @Test
  public void testMultiplication() throws FileNotFoundException, IllegalArgumentException {
    expression.parse("resources/multiplication.txt");
    assertThat(expression.calculate(), closeTo(33 * 4, epsilon));
    expression.clearExpression();

    expression.parse("resources/multiplication2.txt");
    assertThat(expression.calculate(), closeTo(3 * 4 * 45, epsilon));
    expression.clearExpression();
  }

  @Test
  public void testDivision() throws FileNotFoundException, IllegalArgumentException {
    expression.parse("resources/division.txt");
    assertThat(expression.calculate(), closeTo(44D / 5, epsilon));
    expression.clearExpression();

    expression.parse("resources/division2.txt");
    assertThat(expression.calculate(), closeTo(4D / 5D / 3, epsilon));
    expression.clearExpression();
  }
}
