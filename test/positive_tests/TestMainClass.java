package positive_tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;

import org.junit.BeforeClass;
import org.junit.Test;

import program.Program;

public class TestMainClass {
  private static double epsilon;

  @BeforeClass
  public static void ini() {
    epsilon = 1e-10;
  }

  @Test
  public void testProgram() {
    Program.main(null);
    assertThat(Program.result, closeTo(3D / 4 + 4, epsilon));
  }
}
