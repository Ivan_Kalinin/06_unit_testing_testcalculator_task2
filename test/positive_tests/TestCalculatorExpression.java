package positive_tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;

import java.io.FileNotFoundException;

import org.junit.BeforeClass;
import org.junit.Test;

import arithmetic.ArithmeticExpression;

public class TestCalculatorExpression {
  private static double epsilon;
  private static ArithmeticExpression expression;

  @BeforeClass
  public static void ini() {
    epsilon = 1e-10;
    expression = new ArithmeticExpression();
  }

  @Test
  public void testExpression1() throws FileNotFoundException, IllegalArgumentException {
    expression.parse("resources/expression.txt");
    assertThat(expression.calculate(), closeTo(3D / 4 + 4, epsilon));
    expression.clearExpression();
  }

  @Test
  public void testExpression2() throws FileNotFoundException, IllegalArgumentException {
    expression.parse("resources/expression2.txt");
    assertThat(expression.calculate(), closeTo(1 + 3 * 4 / 3 - 2.5D, epsilon));
    expression.clearExpression();
  }
}
