package negative_tests;

import java.io.FileNotFoundException;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import arithmetic.ArithmeticExpression;

public class TestInvalidOperands {
  private static ArithmeticExpression expression;

  @BeforeClass
  public static void ini() {
    expression = new ArithmeticExpression();
  }
  
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void testUnknownOperand1() throws FileNotFoundException, IllegalArgumentException {
    thrown.expect(IllegalArgumentException.class);
    thrown.expectMessage("was found illegal symbol");
    
    expression.clearExpression();
    expression.parse("resources/unknown_operand1.txt");
    expression.calculate();
  }
  
  @Test
  public void testUnknownOperand2() throws FileNotFoundException, IllegalArgumentException {
    thrown.expect(IllegalArgumentException.class);
    thrown.expectMessage("was found illegal symbol");
    
    expression.clearExpression();
    expression.parse("resources/unknown_operand2.txt");
    expression.calculate();
  }
}
