package negative_tests;

import java.io.FileNotFoundException;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import arithmetic.ArithmeticExpression;

public class TestRejectStates {
  private static ArithmeticExpression expression;

  @BeforeClass
  public static void ini() {
    expression = new ArithmeticExpression();
  }
  
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void testDoubleOperand() throws FileNotFoundException, IllegalArgumentException {
    thrown.expect(IllegalArgumentException.class);
    thrown.expectMessage("excess operand");
    
    expression.clearExpression();
    expression.parse("resources/double_operand.txt");
    expression.calculate();
  }
  
  @Test
  public void testDoubleOperator() throws FileNotFoundException {
    thrown.expect(IllegalArgumentException.class);
    thrown.expectMessage("excess operator");
    
    expression.clearExpression();
    expression.parse("resources/double_operator.txt");
    expression.calculate();
  }
  
  @Test
  public void testEmptyString() throws FileNotFoundException, IllegalArgumentException {
    thrown.expect(IllegalArgumentException.class);
    thrown.expectMessage("empty string");
    
    expression.clearExpression();
    expression.parse("resources/empty.txt");
    expression.calculate();
  }
  
  @Test
  public void testDivideByZero() throws FileNotFoundException, IllegalArgumentException {
    thrown.expect(IllegalArgumentException.class);
    thrown.expectMessage("divide by zero");
    
    expression.parse("resources/dividebyzero.txt");
    expression.calculate();
  }
}
